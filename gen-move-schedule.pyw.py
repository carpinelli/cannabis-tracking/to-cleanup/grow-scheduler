#!/usr/bin/env python3

"""Generate a veg to bloom move schedule for the week."""


# Generate schedules containing the rooms and cut-off points for each
# day of the week. This includes the room being moved into, the number
# of trays to move, the number of plants, and the totals for the trays
# and number of plants.

# There are two main tables:

# Schedule:
# index=Location, columns={trays, plants}
# Special location: All/Total

# Metadata:
# index={title: "Veg to Bloom Move",
#        day of the week,
#        date("%m/%d/%Y),
#        plants per tray}

# Unused table: Strain Medium
# Strain/#, Strain/#, Strain/#, Medium

# How do you build a move schedule without Python
# i.e. how would a human do it?
#
# Figure out the order of the rooms
# Figure out the total number of trays per move
# Figure otu the plants per tray
#
# Figure out which room was placed last
# Determine if the room was filled, or if it was only partially filled
# If the room was only partially filled:
#   Start move off in the room that was placed last
#   and Start with the number of empty trays in that room to finish it off
# Else if the room was fully placed:
#   Start the move off in the next room with a full amount of trays
# Continue adding the next room, and using all available trays until the total
# number needed per move has been met
