#!/usr/bin/env python3

"""Generate weekly schedules."""

import datetime
import json
from pathlib import Path

import openpyxl
import pymsgbox

from IPython.core.debugger import set_trace


# Module Constants
SHEET_NAMES = ("Monday", "Tuesday",
               "Wednesday", "Thursday",
               "Friday")
ROOM_COL = 'A'
TRAY_COL = 'B'
COUNT_COL = 'C'
INFO_COL = 'G'
START_ROW = 1  # index, not actual row
DATE_ROW = 1
PLANTS_TRAY_ROW = 4
TOTAL_IDX = 12
TOTAL_HEADER = "TOTAL"
BLANKS = (None, "", [], (), {})
# Boolean
YES_ANSWERS = ['y', 'Y', '']
NO_ANSWERS = ['n', 'N']
# Files/Path
ROOMS_JSON = str(Path("data/Bloom.json").resolve())
TRAYS_PER_DAY_JSON = str(Path("data/Trays per Day.json").resolve())
TRAYS_PER_ROOM_JSON = str(Path("data/Trays per Room.json").resolve())
EMPTY_MOVE_SCHEDULE = str(Path("data/Move Schedule (Empty).xlsx").resolve())
# # Load information from file # #
with open(ROOMS_JSON) as json_file:
    ROOMS = json.load(json_file)  # list
with open(TRAYS_PER_DAY_JSON) as json_file:
    TRAYS_PER_DAY = json.load(json_file)  # int
with open(TRAYS_PER_ROOM_JSON) as json_file:
    TRAYS_PER_ROOM = json.load(json_file)  # dict


def next_monday():
    """Get the date for the next occurring Monday."""
    # ((Day - Today) + 7) % 7 give the weekday index
    days_after = 7
    today = datetime.date.today()
    days_ahead = days_after - today.weekday()
    return today + datetime.timedelta(days=days_ahead)


def input_last_move() -> dict:
    """Ask which room was last placed, then if the room was fully or
    partially moved. If fully moved, remaining trays is set to
    0. If partially moved, ask how many trays were placed, then use
    that number and the number of trays in the room to calculate
    remaining trays.
    :return a dict containing the last room and remaining trays."""
    # Get last room placed
    input_room_msg = "Enter the last room placed at the end of this week: "
    last_room = pymsgbox.prompt(input_room_msg).upper()
    # While input answer is not valid
    while True:
        full = pymsgbox.prompt("Was it a full room? Y/n: ")
        if full in YES_ANSWERS:
            return {"Room": last_room, "Trays": 0}  # 0 remaining_trays
        elif full in NO_ANSWERS:
            input_trays_msg = "Enter the number of trays already placed: "
            trays_placed = int(pymsgbox.prompt(input_trays_msg))
            # Calculate remaining trays
            return {"Room": last_room,
                    "Trays": int(TRAYS_PER_ROOM[last_room]) - trays_placed}
        else:
            pymsgbox.alert("Option not recognized. Trying again...")
            # continue


def build_schedule(plants_per_tray: int) -> dict:
    # Values used to build return dict
    rooms = list()
    tray_counts = list()
    plant_counts = list()
    # Just starting so use a full day worth of remaining trays
    remaining_trays = TRAYS_PER_DAY
    # Check if the previous room was a partial and determine if trays
    # still need to be placed
    last_move = input_last_move()

    # ##### BUG IN THIS CHECK, USING last_move["Room"]
    # when full leads to reuse of last room. use current room
    # TMP FIX by setting last_move["Trays"] = 0 in break
    # ##### BUG ######
    # Check if the previous room was a partial
    # if so, add remaining trays from previous partial
    if last_move["Trays"] > 0:
        plant_count = last_move["Trays"] * plants_per_tray

        # Build partial entry
        rooms.append(last_move["Room"])
        tray_counts.append(last_move["Trays"])
        plant_counts.append(plant_count)

        # Update totals used to track and control progress
        remaining_trays -= last_move["Trays"]
        # last_move["Trays"] = 0  # Empty last_move

    # Build schedule information
    while True:  # remaining_trays > 0:
        # Get index of last room used
        last_room_idx = ROOMS.index(last_move["Room"])

        # Check if last_room is at the end of the list
        # if so start at the beginning, else go to the next index
        # In other words: reset index, if needed
        current_room_idx = (0 if (last_room_idx == (len(ROOMS) - 1))
                            else last_room_idx + 1)

        # See line 171 on... just append these to the table,
        # instead of storing them
        # Build rooms, tray counts, and plant counts
        current_room = ROOMS[current_room_idx]
        last_move["Trays"] = int(TRAYS_PER_ROOM[current_room])

        ###### ERROR ######
        # CALCULATING last_move["Trays"] wrong on update day
        # 6, 4.A.2, 4: Input to replicate
        ###### ERROR ######
        #### ERROR UPDATE ####
        # Now it is not finish a room
        # i.e. above leads to c.1 only being filled with 12 plants...,
        # then nothing on the next day
        #### ERROR UPDATE ####

        # Last room to schedule?
        # Determine if remaining trays in room are greater than
        # the number of total remaining trays
        difference = remaining_trays - last_move["Trays"]
        if difference >= 0:  # At least 1 more room to schedule
            plant_count = last_move["Trays"] * plants_per_tray  # Using a variable to hold if or else value
            tray_count = last_move["Trays"]
            remaining_trays -= last_move["Trays"]
            # last_move["Trays"] -= last_move["Trays"]  # UNSURE!
        else:  # This is the last room to schedule
            plant_count = remaining_trays * plants_per_tray  # Using a variable to hold if or else value
            # Update tray count then empty 'remaining_trays'
            tray_count, remaining_trays = (remaining_trays, 0)
            last_move["Trays"] = (int(TRAYS_PER_ROOM[current_room])
                                  - tray_count)

        # Actual appending of data for the day
        # Add rooms, tray counts, and plant counts to spreadsheet
        rooms.append(current_room)
        tray_counts.append(tray_count)
        plant_counts.append(plant_count)

        # Update values for next iteration
        last_move["Room"] = current_room
        current_room_idx += 1

        # Done building schedule for the active day
        if remaining_trays <= 0:
            last_move["Trays"] = 0  # Avoid reusing this room on next day
            break


def schedules_totals() -> dict:
    """Build actual schedule information for each day of the
    week.
    :return A dict containing the schedule sheets, totals for
    each week, and the plants per tray for this move."""
    # # Get User Input # #
    # Get plants per tray
    # Validate input
    plants_per_tray = int(pymsgbox.prompt("Enter the plants per tray: "))

    # # Build a Sheet for Each Day # #
    # Used to build the return dict
    totals = dict(dict())
    schedule_sheets = dict(dict())
    for day in SHEET_NAMES:
        schedule = build_sheet(plants_per_tray)

        schedule_list.append(schedule["Day"])
        total_list.append(schedule["Total"])
        """
        schedule_sheets.append(build_schedule_sheet(day))
        totals.append(build_total_sheet(day))
        """
        # Build-out final data/format
        totals.update({day: {"Trays": sum(tray_counts),
                             "Plants": sum(plant_counts)}})
        schedule_sheets.update({day: {"Rooms": scheduled_rooms,
                                      "Trays": tray_counts,
                                      "Plants": plant_counts}})
    # # Debug # #
    from pprint import pprint
    set_trace()
    pprint(schedule_sheets)
    # # Debug # #
    return {"Schedules": schedule_sheets,
            "Totals": totals,
            # "Date": date,
            "Plants per Tray": plants_per_tray}


def save_move_schedule(move: dict) -> None:
    """Write built-out schedule to file.
    # Place save in try/except block
    # optimize? read_only?
    """
    current_date = next_monday()  # Date
    date = f"{current_date.month}/{current_date.day}/{current_date.year}"
    # Change separator in date
    output_schedule = str(Path(f"{date.replace('/', '.')} Move Cycle.xlsx"))
    # Open empty workbook
    schedule_wb = openpyxl.load_workbook(EMPTY_MOVE_SCHEDULE)
    # Write generated schedule to Excel file
    for day in SHEET_NAMES:
        schedule_ws = schedule_wb[day]

        # Write each column individually, one row at a time
        # Add rooms, trays, and counts
        row = START_ROW
        for room in move["Schedules"][day]["Rooms"]:
            schedule_ws[ROOM_COL][row].value = room
            row += 1
        row = START_ROW
        for tray_count in move["Schedules"][day]["Trays"]:
            schedule_ws[TRAY_COL][row].value = tray_count
            row += 1
        row = START_ROW
        for plant_count in move["Schedules"][day]["Plants"]:
            schedule_ws[COUNT_COL][row].value = plant_count
            row += 1

        # # Write to sections other than explicit schedule # #
        # Write final row as total-values
        # Add totals
        schedule_ws[TRAY_COL][TOTAL_IDX].value = move["Totals"][day]["Trays"]
        schedule_ws[COUNT_COL][TOTAL_IDX].value = move["Totals"][day]["Plants"]
        # Write date to date section
        schedule_ws[INFO_COL][DATE_ROW].value = date

        # Add plants per tray to proper section
        schedule_ws[INFO_COL][PLANTS_TRAY_ROW].value = move["Plants per Tray"]

        # # Delete blank lines # #
        # Blank row count loop setup
        next_row = START_ROW + len(move["Schedules"][day]["Rooms"])
        row = next_row  # Aliasing
        value = schedule_ws[ROOM_COL][row].value  # Pull/check current value
        blank_count = 0
        # Count blank lines
        while value != TOTAL_HEADER:
            if value in BLANKS:
                blank_count += 1
            row += 1
            # Pull/check current value
            value = schedule_ws[ROOM_COL][row].value
        # Delete counted blank rows
        if blank_count > 0:
            schedule_ws.delete_rows((next_row + 1), blank_count)  # (row, n)
            # if blank_count == 0, then it deletes everything below,
            # including the Totals row

        current_date += datetime.timedelta(days=1)  # Increment date

    # Place in try/catch/except block
    schedule_wb.save(output_schedule)  # Final write/save
    pymsgbox.alert(f"Output saved to {output_schedule}")

    return None


def gen_move_schedule() -> None:
    """Generate a move schedule for the week and save it to an Excel
    file.
    # Date/Room overlap error."""
    # Build actual schedule information for each day of the week
    move = schedules_totals()
    # Write to and save as an Excel workbook
    save_move_schedule(move)
    return None


def main() -> int:
    gen_move_schedule()
    return 0


if __name__ == "__main__":
    main()
